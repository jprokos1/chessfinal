CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


chess: main.o Board.o Chess.o CreatePiece.o Pawn.o Rook.o Knight.o Bishop.o Queen.o King.o Mystery.o
	$(CC) -o chess main.o Board.o Chess.o CreatePiece.o Pawn.o Rook.o Knight.o Bishop.o Queen.o King.o Mystery.o

Board.o: Board.cpp Bishop.h Board.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h Terminal.h
	$(CC) -c Board.cpp $(CFLAGS)

Chess.o: Chess.cpp Board.h Chess.h Piece.h
	$(CC) -c Chess.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp Bishop.h Board.h Chess.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h
	$(CC) -c CreatePiece.cpp $(CFLAGS)

King.o: King.h King.cpp Piece.h
	$(CC) -c King.cpp $(CFLAGS)

Queen.o: Queen.h Queen.cpp Piece.h
	$(CC) -c Queen.cpp $(CFLAGS)

Rook.o: Rook.h Rook.cpp Piece.h
	$(CC) -c Rook.cpp $(CFLAGS)

Bishop.o: Bishop.h Bishop.cpp Piece.h
	$(CC) -c Bishop.cpp $(CFLAGS)

Pawn.o: Pawn.h Pawn.cpp Piece.h
	$(CC) -c Pawn.cpp $(CFLAGS)

Knight.o: Knight.h Knight.cpp Piece.h
	$(CC) -c Knight.cpp $(CFLAGS)

Mystery.o: Mystery.h Piece.h
	$(CC) -c Mystery.cpp $(CFLAGS)

main.o: Bishop.o Board.h Chess.h King.o Knight.o Mystery.o Pawn.o Piece.h CreatePiece.h Queen.o Rook.o Terminal.h
	$(CC) -c main.cpp $(CFLAGS)

.PHONY: clean all
clean:
	rm -f *.o chess
