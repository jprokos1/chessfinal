#include "King.h"
#include <cmath>

using std::pair;
using std::abs;

bool King::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  //Larger than 1 space
  if (abs(end.first - start.first) > 1 ||
      abs(end.second - start.second) > 1) {
    return false;
  }
  //Valid
  return true;
}
/*
bool King::legal_capture_shape(pair<char, char> start, pair<char, char> end) const {
  //Check if valid move shape
  if (!legal_move_shape(start, end)) { return false; }
  bool color = is_white; //color of piece
  map<pair<char, char>, Piece*> b = getOcc(); //map of the board
  if (b[end].is_white == col) { //check if piece being captured is same color
    return false;
  }
  return true;
}
*/
