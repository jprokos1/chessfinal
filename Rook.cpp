#include "Rook.h"

using std::pair;

//Rook can only move in straight line
bool Rook::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  if(start.first == end.first)
    return true;
  if(start.second == end.second)
    return true;
  return false;
}
