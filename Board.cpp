#include <iostream>
#include <utility>
#include <map>
#include <vector>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"

using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::make_pair;
using std::vector;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

//Constructor for Board
Board::Board( const Board& b ) {
  for (map<std::pair<char, char>, Piece*>::const_iterator it = b._occ.cbegin();
       it != b._occ.cend();
       it++) {
    Piece* copyPiece = create_piece(it->second->to_ascii());
    _occ[it->first] = copyPiece;
  }
}

//Destructor for Board
Board::~Board(void) {
  vector<pair<char, char>> to_delete;
  for (map<std::pair<char, char>, Piece*>::const_iterator it = _occ.cbegin();
       it != _occ.cend();
       it++) {
    to_delete.push_back(it->first);
  }
  for (vector<pair<char, char>>::iterator it = to_delete.begin(); it!=to_delete.end();it++) {
    remove_piece(*it);
  }
}

//This returns the given color's king position
pair<char, char> Board::getKingPos(bool white) {
  char kingColor;
  if (white) { kingColor = 'K'; }
  else { kingColor = 'k'; }

  pair<char, char> kingPos;

  for (map<pair<char, char>, Piece*>::const_iterator it = _occ.cbegin();
       it != _occ.cend();
       it++) {
    if ((it->second)->to_ascii() == kingColor) {
      kingPos = it->first;
    }
  }
  return kingPos;
}

//This returns the piece at the position
const Piece* Board::operator()( std::pair< char , char > position ) const
{
  if (_occ.find(position) == _occ.end()) {
    return NULL;
  }
  return _occ.at(position);
}

//This adds the piece to the board as long as the position is valid
bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  if(position.first > 'H' || position.first < 'A' || position.second > '8' || position.first < '1') {
    return false;
  }
  if(_occ.find(position) != _occ.end()) {
    return false;
  }
  _occ[ position ] = create_piece( piece_designator );
  return _occ[position] != NULL;
}

//This removes the piece at the position
void Board::remove_piece(std::pair<char, char> position) {
  if (_occ.find(position) != _occ.end()) {
    delete _occ[position];
    }
  
  _occ.erase(position);
}

//This returns whether the board has 2 kings (one white and one black)
bool Board::has_valid_kings( void ) const
{
  int wk = 0;
  int bk = 0;
  for(map<pair<char, char>, Piece*>::const_iterator it = _occ.cbegin(); it != _occ.cend(); it++) {
    Piece * curPiece = it->second;
    if(curPiece->to_ascii() == 'k') {
      bk++;
    }
    if(curPiece->to_ascii() == 'K') {
      wk++;
    }
  }
  return ((bk == 1) && (wk == 1));
}

//Displays the current board
void Board::display( void ) const
{
  cout << "   ";
  for (char i = 'A'; i <= 'H'; i++) {
    cout << ' ' << i << ' ';
  }
  cout << endl;
  bool alternate = true;
  for (char j = '8'; j >= '1'; j--) {
    alternate = !alternate;
    cout << ' ' << j << ' ';
    for (char i = 'A'; i <= 'H'; i++) {
      if (alternate) {
	Terminal::color_bg(Terminal::CYAN);
	alternate = !alternate;
      } else {
	Terminal::color_bg(Terminal::RED);
	alternate = !alternate;
      }
      pair<char, char> pos = make_pair(i, j);
      Piece* piece;
      if (this->_occ.find(pos) == _occ.end()) {
	piece = NULL;
      } else {
	piece = this->_occ.at(pos);
      }
      if (piece == NULL) {
	Terminal::color_fg(false, Terminal::RED);
      }
      else if (piece->is_white()) {
	Terminal::color_fg(false, Terminal::WHITE);
      } else {
	Terminal::color_fg(false, Terminal::BLACK);
      }
      if (piece == NULL) {
	cout << "   ";
      } else {
	cout << " " << piece->to_ascii() << " ";
      }
    }
    Terminal::set_default();
    cout << endl;
    }
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
