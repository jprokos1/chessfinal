#include "Bishop.h"
#include <cmath>

using std::pair;
using std::abs;

//Bishop can only move diagonally (difference between the row and columns are the same
bool Bishop::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  if(abs(start.first - end.first) == abs(start.second - end.second))
    return true;
  return false;
}
