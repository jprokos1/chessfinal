#include "Chess.h"
#include <cassert>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

//Checks if there is a piece in the way
//Returns true if it is clear, false if not
bool Chess::checkPath(std::pair<char, char> start, std::pair<char, char> end) {
  //distance of each direction on the board
  int rowDist = end.first - start.first;
  int colDist = end.second - start.second;
  //Getting how much to iterate our board checking based off of dist
  if(rowDist < 0) {
    rowDist = -1;
  }
  else if (rowDist > 0){
    rowDist = 1;
  }
  if(colDist < 0) {
    colDist = -1;
  }
  else if (colDist > 0){
    colDist = 1;
  }
  bool straightLine = false;
  if ((std::abs(end.first - start.first) == std::abs(end.second - start.second)) || //Diagonal
      (std::abs(end.first - start.first) == 0 || //Vertical or Horizontal
       std::abs(end.second - start.second) == 0)) {
    straightLine = true;
  }
  //Piece jumps
  if (!straightLine) {
    return true;
  }
  std::pair<char, char> curPos = start;
  curPos.first += rowDist;
  curPos.second += colDist;
  while(curPos != end) {
    if (_board(curPos) != NULL) {
      return false;
    }
    curPos.first += rowDist;
    curPos.second += colDist;
  }
  return true;
}

bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{

  //Check if out of bounds
  if (start.first < 'A' || end.first < 'A' ||
      start.first > 'H' || end.first > 'H' ||
      start.second < '1' || end.second < '1' ||
      start.second > '8' || end.second > '8') {
    return false;
  }

  
  //No piece at start
  if (_board(start) == NULL) {
    return false;
  }
  const Piece* startPiece = _board(start);
  const Piece* endPiece = _board(end);
  bool legal = startPiece->legal_move_shape(start, end);
  bool startColor = startPiece->is_white();
  bool endRemove = false;
  char startAscii = startPiece->to_ascii();
  char endAscii;

  if (startColor != _turn_white) {
    return false;
  }

  if (startPiece == NULL) {
    std::cout << "start piece false";
    return false;
  }
  
  if (legal) {
    //Check if Pieces in way
    if (!checkPath(start, end)) {
      return false;
    }
    //Piece to remove
    if (_board(end) != NULL) {
      endRemove = true;
      endAscii = endPiece->to_ascii();
      //Same color
      bool endColor = _board(end)->is_white();
      if (startColor == endColor) {
	return false;
      }
      _board.remove_piece(end);
    }
    //Legal Capture
    if (startPiece->to_ascii() == 'p' || startPiece->to_ascii() == 'P') {
      //Promote
      if(startColor && end.second == '8') {
	_board.remove_piece(start);
	_board.add_piece(end, 'Q');
      }
      if(!startColor && end.second == '1') {
	_board.remove_piece(start);
	_board.add_piece(end, 'q');
      }
    }
    
    _board.remove_piece(start);
    //Add piece to capture spot and remove captured piece, if applicable
    _board.add_piece(end, startAscii);
    if (in_check(startColor)) {
      _board.remove_piece(end);
      _board.add_piece(start, startAscii);
      if (endRemove) {
	_board.add_piece(end, endAscii);
      }
      return false;
    }
    _turn_white = (!_turn_white);
    return true;
  }
  else if (startPiece->legal_capture_shape(start, end)) { //for pawn
    //No Piece there
    if (_board(end) == NULL) {
      return false;
    }
    //Same Color
    bool endColor = _board(end)->is_white();
    if (startColor == endColor) {
      return false;
    }
    //Promote
    if(startColor && end.second == '8') {
      _board.remove_piece(start);
      _board.add_piece(end, 'Q');
    }
    if(!startColor && end.second == '1') {
      _board.remove_piece(start);
      _board.add_piece(end, 'q');
    }
    //Legal capture for pawn
    _board.remove_piece(start);
    _board.remove_piece(end);
    _board.add_piece(end, startAscii);
    _turn_white = (!_turn_white);
    if (in_check(startColor)) {
      _board.remove_piece(end);
      _board.add_piece(start, startAscii);
      return false;
    }
    return true;
  }
  return false;
}
bool Chess::in_check( bool white ) const
{
  //Copies the board and finds if there's a piece that can capture king
  Chess copy = Chess(*this);
  std::pair<char,char> kingPos;
  kingPos = copy._board.getKingPos(white);
  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '1'; j <= '8'; j++) {
      std::pair<char, char> loc = std::make_pair(i, j);
      const Piece* curPiece = copy._board(loc);
      if (curPiece != NULL) {
	bool color = curPiece->is_white();
	if (color != white) {
	  if ((curPiece->legal_capture_shape(loc, kingPos)) && copy.checkPath(loc, kingPos)) {
	    return true;
	  }
	}
      }
    }
  }
  return false;
}
bool Chess::in_mate( bool white ) const
{
  return in_check(white) && in_stalemate(white);
}

bool Chess::in_stalemate( bool white ) const
{
  //Copies the board and tests if any piece of your color can move
  Chess copy(*this);
  Board b = copy._board;

  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '1'; j <= '8'; j++) {
      std::pair<char, char> loc = std::make_pair(i, j);
      const Piece* curPiece = copy._board(loc);
      if (curPiece != NULL) {
	bool color = curPiece->is_white();
	if (color == white) {
	for (char x = 'A'; x <= 'H'; x++) {
	  for (char y = '1'; y <= '8'; y++) {
	    std::pair<char, char> endLoc = std::make_pair(x, y);
	    if (copy.make_move(loc, endLoc)) {
	      return false;
	    }
	  }
	}
	}
      }
    }
  }
  
  return true;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  for (char i = 'A'; i <= 'H'; i++) {
    for (char j = '8'; j >= '1'; j--) {
      chess.getBoard().remove_piece(std::pair<char, char>(i,j));
    }
  }
  char in;
  for(char i = '8'; i >= '1'; i--) {
    for(char j = 'A'; j <= 'H'; j++) {
      is >> in;
      if(!(in == '-')) {
	chess.getBoard().add_piece(std::pair<char, char>(j, i), in);
      }
    }
  }
  is >> in;
  if(in == 'b') {
    chess.setTurn(false);
  }
  else if(in == 'w') {
    chess.setTurn(true);
  }
  else {
    assert(false);
  }
  return is;
}
