#include "Queen.h"
#include <cmath>

using std::pair;

//Queen has the combined movement of rook and bishop added together
bool Queen::legal_move_shape(pair<char, char> start, pair<char, char> end) const {
  //Diagonal
  if(std::abs(start.first - end.first) == std::abs(start.second - end.second)) {
    return true;
  }
  //Vertical
  else if(start.first == end.first) {
    return true;
  }
  //Horizontal
  else if(start.second == end.second) {
    return true;
  }
  else {
    return false;
  }
}
