//Pawn.cpp
#include "Pawn.h"

using std::pair;

//All Pieces are running on the assumptionn that the positions are valid with a piece at the start,
//Which is checked in make_move.
bool Pawn::legal_move_shape(pair<char,char> start, pair<char, char> end) const {
  int yDist = end.second - start.second;
  int xDist = end.first - start.first;
  if(is_white()) {
    if(xDist != 0) {
      return false;
    }
    //Check if the pawn is in the starting position, allowing for it to go 1 or 2 forward
    if(start.second == '2') {
      return yDist == 1 || yDist == 2;
    }
    else {
      return yDist == 1;
    }
  }
  else {
    if(xDist != 0) {
      return false;
    }
    //Check if the pawn is in the starting position, allowing for it to go 1 or 2 forward
    if(start.second == '7') {
      return yDist == -1 || yDist == -2;
    }
    else {
      return yDist == -1;
    }
  }
  return false;
}

bool Pawn::legal_capture_shape(pair<char,char> start, pair<char,char> end) const {
  int yDist = end.second - start.second;
  int xDist = end.first - start.first;
  if(is_white()) {
    return yDist == 1 && (xDist == 1 || xDist == -1);
  }
  else {
    return yDist == -1 && (xDist == 1 || xDist == -1);
  }

  return false;
}
