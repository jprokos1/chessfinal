//Knight.cpp
#include "Knight.h"
using std:: pair;
//Moves in an L shape. Assumes the bounds are already valid.
bool Knight::legal_move_shape(pair<char,char> start, pair<char,char> end) const {
  //How much the knight moves along the rows and columns
  int rowDist = end.first - start.first;
  int colDist = end.second - start.second;
  if(rowDist < 0) {
    rowDist *= -1;
  }
  if(colDist < 0) {
    colDist *= -1;
  }
  if(colDist == 1) {
    return rowDist == 2;
  }
  if(rowDist == 1) {
    return colDist == 2;
  }
  return false;
}
